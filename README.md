Nodeletter
==========

Nodeletter module is created to allow users to create and send newsletters via third party service providers directly from within Drupal.
It takes information of an ordinary content node (e.g. field values) and pushes it into pre-defined templates on the third party service.
The users don't see or feel the presence and utilisation of the third party service.


